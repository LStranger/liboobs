
/* Generated data (by glib-mkenums) */

#include "oobs-enum-types.h"

/* enumerations from "oobs-error.h" */
#include "oobs-error.h"

GType
oobs_error_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_ERROR_AUTHENTICATION_FAILED, "OOBS_ERROR_AUTHENTICATION_FAILED", "failed" },
        { OOBS_ERROR_AUTHENTICATION_CANCELLED, "OOBS_ERROR_AUTHENTICATION_CANCELLED", "cancelled" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsError"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-share-smb.h" */
#include "oobs-share-smb.h"

GType
oobs_share_smb_flags_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GFlagsValue values[] = {
        { OOBS_SHARE_SMB_ENABLED, "OOBS_SHARE_SMB_ENABLED", "enabled" },
        { OOBS_SHARE_SMB_BROWSABLE, "OOBS_SHARE_SMB_BROWSABLE", "browsable" },
        { OOBS_SHARE_SMB_PUBLIC, "OOBS_SHARE_SMB_PUBLIC", "public" },
        { OOBS_SHARE_SMB_WRITABLE, "OOBS_SHARE_SMB_WRITABLE", "writable" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_flags_register_static (g_intern_static_string ("OobsShareSMBFlags"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-user.h" */
#include "oobs-user.h"

GType
oobs_user_home_flags_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GFlagsValue values[] = {
        { OOBS_USER_REMOVE_HOME, "OOBS_USER_REMOVE_HOME", "remove-home" },
        { OOBS_USER_CHOWN_HOME, "OOBS_USER_CHOWN_HOME", "chown-home" },
        { OOBS_USER_COPY_HOME, "OOBS_USER_COPY_HOME", "copy-home" },
        { OOBS_USER_ERASE_HOME, "OOBS_USER_ERASE_HOME", "erase-home" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_flags_register_static (g_intern_static_string ("OobsUserHomeFlags"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-ifacesconfig.h" */
#include "oobs-ifacesconfig.h"

GType
oobs_iface_type_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_IFACE_TYPE_ETHERNET, "OOBS_IFACE_TYPE_ETHERNET", "ethernet" },
        { OOBS_IFACE_TYPE_WIRELESS, "OOBS_IFACE_TYPE_WIRELESS", "wireless" },
        { OOBS_IFACE_TYPE_IRLAN, "OOBS_IFACE_TYPE_IRLAN", "irlan" },
        { OOBS_IFACE_TYPE_PLIP, "OOBS_IFACE_TYPE_PLIP", "plip" },
        { OOBS_IFACE_TYPE_PPP, "OOBS_IFACE_TYPE_PPP", "ppp" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsIfaceType"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-iface-ppp.h" */
#include "oobs-iface-ppp.h"

GType
oobs_modem_volume_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_MODEM_VOLUME_SILENT, "OOBS_MODEM_VOLUME_SILENT", "silent" },
        { OOBS_MODEM_VOLUME_LOW, "OOBS_MODEM_VOLUME_LOW", "low" },
        { OOBS_MODEM_VOLUME_MEDIUM, "OOBS_MODEM_VOLUME_MEDIUM", "medium" },
        { OOBS_MODEM_VOLUME_LOUD, "OOBS_MODEM_VOLUME_LOUD", "loud" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsModemVolume"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}
GType
oobs_dial_type_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_DIAL_TYPE_TONES, "OOBS_DIAL_TYPE_TONES", "tones" },
        { OOBS_DIAL_TYPE_PULSES, "OOBS_DIAL_TYPE_PULSES", "pulses" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsDialType"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-servicesconfig.h" */
#include "oobs-servicesconfig.h"

GType
oobs_runlevel_role_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_RUNLEVEL_HALT, "OOBS_RUNLEVEL_HALT", "halt" },
        { OOBS_RUNLEVEL_REBOOT, "OOBS_RUNLEVEL_REBOOT", "reboot" },
        { OOBS_RUNLEVEL_MONOUSER, "OOBS_RUNLEVEL_MONOUSER", "monouser" },
        { OOBS_RUNLEVEL_MULTIUSER, "OOBS_RUNLEVEL_MULTIUSER", "multiuser" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsRunlevelRole"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-service.h" */
#include "oobs-service.h"

GType
oobs_service_status_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_SERVICE_START, "OOBS_SERVICE_START", "start" },
        { OOBS_SERVICE_STOP, "OOBS_SERVICE_STOP", "stop" },
        { OOBS_SERVICE_IGNORE, "OOBS_SERVICE_IGNORE", "ignore" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsServiceStatus"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* enumerations from "oobs-result.h" */
#include "oobs-result.h"

GType
oobs_result_get_type (void)
{
  static volatile gsize g_enum_type_id__volatile = 0;

  if (g_once_init_enter (&g_enum_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { OOBS_RESULT_OK, "OOBS_RESULT_OK", "ok" },
        { OOBS_RESULT_ACCESS_DENIED, "OOBS_RESULT_ACCESS_DENIED", "access-denied" },
        { OOBS_RESULT_NO_PLATFORM, "OOBS_RESULT_NO_PLATFORM", "no-platform" },
        { OOBS_RESULT_MALFORMED_DATA, "OOBS_RESULT_MALFORMED_DATA", "malformed-data" },
        { OOBS_RESULT_ERROR, "OOBS_RESULT_ERROR", "error" },
        { 0, NULL, NULL }
      };
      GType g_enum_type_id;

      g_enum_type_id =
        g_enum_register_static (g_intern_static_string ("OobsResult"), values);

      g_once_init_leave (&g_enum_type_id__volatile, g_enum_type_id);
    }

  return g_enum_type_id__volatile;
}

/* Generated data ends here */

