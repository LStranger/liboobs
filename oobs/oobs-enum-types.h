
/* Generated data (by glib-mkenums) */

#ifndef __OOBS_ENUM_TYPES_H__
#define __OOBS_ENUM_TYPES_H__

#include <glib-object.h>

G_BEGIN_DECLS

/* enumerations from "oobs-error.h" */
GType oobs_error_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_ERROR (oobs_error_get_type())

/* enumerations from "oobs-share-smb.h" */
GType oobs_share_smb_flags_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_SHARE_SMB_FLAGS (oobs_share_smb_flags_get_type())

/* enumerations from "oobs-user.h" */
GType oobs_user_home_flags_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_USER_HOME_FLAGS (oobs_user_home_flags_get_type())

/* enumerations from "oobs-ifacesconfig.h" */
GType oobs_iface_type_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_IFACE_TYPE (oobs_iface_type_get_type())

/* enumerations from "oobs-iface-ppp.h" */
GType oobs_modem_volume_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_MODEM_VOLUME (oobs_modem_volume_get_type())

GType oobs_dial_type_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_DIAL_TYPE (oobs_dial_type_get_type())

/* enumerations from "oobs-servicesconfig.h" */
GType oobs_runlevel_role_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_RUNLEVEL_ROLE (oobs_runlevel_role_get_type())

/* enumerations from "oobs-service.h" */
GType oobs_service_status_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_SERVICE_STATUS (oobs_service_status_get_type())

/* enumerations from "oobs-result.h" */
GType oobs_result_get_type (void) G_GNUC_CONST;
#define OOBS_TYPE_RESULT (oobs_result_get_type())

G_END_DECLS

#endif /* !__MUTTER_ENUM_TYPES_H__ */

/* Generated data ends here */

